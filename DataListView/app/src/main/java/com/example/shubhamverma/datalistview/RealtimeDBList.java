package com.example.shubhamverma.datalistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

public class RealtimeDBList extends AppCompatActivity {

    private ListView dataListView;
    private EditText itemText;
    private Button findButton;
    private Button addButton;
    private Button deleteButton;
    private Boolean searchMode = false;
    private Boolean itemSelected = false;
    private int selectedPosition = 0;
    private FirebaseDatabase database;
    private DatabaseReference dbref;



    ArrayList<String> listItems = new ArrayList<String>();
    ArrayList<String> listKeys = new ArrayList<String>();
    ArrayAdapter<String> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realtime_dblist);

        dataListView = (ListView)findViewById(R.id.dataListView);
        findButton = (Button)findViewById(R.id.findButton);
        deleteButton = (Button)findViewById(R.id.deleteButton);
        itemText = (EditText)findViewById(R.id.itemText);
        addButton = (Button)findViewById(R.id.addButton);


        FirebaseApp.initializeApp(this);

        database = FirebaseDatabase.getInstance();
        dbref = database.getReference("todo");
        deleteButton.setEnabled(false);

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_single_choice,listItems);

        dataListView.setAdapter(adapter);
        dataListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        dataListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPosition = i;
                itemSelected = true;
                deleteButton.setEnabled(true);
            }
        });

        addButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String item = itemText.getText().toString();
                String key = dbref.push().getKey();

                itemText.setText("");
                dbref.child(key).child("description").setValue(item);

                adapter.notifyDataSetChanged();
            }
        });


        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataListView.setItemChecked(selectedPosition, false);
                dbref.child(listKeys.get(selectedPosition)).removeValue();
            }
        });

        findButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query query;

                if(!searchMode) {
                    findButton.setText("Clear");
                    query = dbref.orderByChild("description").equalTo(itemText.getText().toString());
                    searchMode = true;

                }else {
                    searchMode = false;
                    findButton.setText("Find");
                    query = dbref.orderByKey();
                }
                if (itemSelected) {
                    dataListView.setItemChecked(selectedPosition, false);
                    itemSelected = false;
                    deleteButton.setEnabled(false);
                }

                query.addListenerForSingleValueEvent(queryValueListener);
            }
        });
        addChildEventListener();
    }

    private void addChildEventListener() {
        ChildEventListener childListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                adapter.add(
                        (String)dataSnapshot.child("description").getValue());
                        listKeys.add(dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getKey();
                int index = listKeys.indexOf(key);

                if(index != -1) {
                    listItems.remove(index);
                    listKeys.remove(index);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        dbref.addChildEventListener(childListener);
    }




    ValueEventListener queryValueListener =  new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            Iterable<DataSnapshot> snapshotIterator = dataSnapshot.getChildren();
            Iterator<DataSnapshot> iterator =  snapshotIterator.iterator();

            adapter.clear();
            listKeys.clear();

            while (iterator.hasNext()) {
                DataSnapshot next = (DataSnapshot) iterator.next();

                String match = (String)next.child("description").getValue();
                String key = next.getKey();

                listKeys.add(key);
                adapter.add(match);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
}
