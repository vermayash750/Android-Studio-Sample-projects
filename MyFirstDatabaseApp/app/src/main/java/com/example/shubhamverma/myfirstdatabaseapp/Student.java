package com.example.shubhamverma.myfirstdatabaseapp;

/**
 * Created by shubham verma on 29-Mar-18.
 */

public class Student {
    private int _studentID;
    private String _studentName;

    public Student()
    {}

    public Student(int id,String studentname)
    {
        this._studentID = id;
        this._studentName = studentname;
    }


    public void setID(int id)
    {
        this._studentID = id;
    }

    public int getID()
    {
        return this._studentID;
    }

    public void setStudentName(String studentname)
    {
        this._studentName = studentname;
    }

    public String getStudentName()
    {
        return this._studentName;
    }
}
