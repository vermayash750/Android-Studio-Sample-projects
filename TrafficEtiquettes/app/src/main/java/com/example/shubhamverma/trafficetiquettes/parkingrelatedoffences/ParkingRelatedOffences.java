package com.example.shubhamverma.trafficetiquettes.parkingrelatedoffences;

public class ParkingRelatedOffences {

    private String section;
    private String penalty;
    private String offence;

    public String getOffence() {
        return offence;
    }

    public void setOffence(String offence) {
        this.offence = offence;
    }

    public String getPenalty() {
        return penalty;
    }

    public void setPenalty(String penalty) {
        this.penalty = penalty;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public ParkingRelatedOffences() {

    }
}
