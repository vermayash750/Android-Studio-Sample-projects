package com.example.shubhamverma.trafficetiquettes.EmergenecyHelplines;

import android.app.ActionBar;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.shubhamverma.trafficetiquettes.R;

public class EmergencyHelplinesActivity extends AppCompatActivity {

    final static String DB_URL= "https://traffic-etiquettes.firebaseio.com/emergenecyHelplinesListItems/";
    ListView listView;
    EmergencyHelplinesFirebase firebase;
    int selectedPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_codes);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView=(ListView)findViewById(R.id.listView);
        firebase= new EmergencyHelplinesFirebase(this, DB_URL,listView);
        firebase.refreshdata();
        }
    }
