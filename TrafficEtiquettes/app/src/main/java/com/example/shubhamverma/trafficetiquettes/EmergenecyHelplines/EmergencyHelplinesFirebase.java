package com.example.shubhamverma.trafficetiquettes.EmergenecyHelplines;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.shubhamverma.trafficetiquettes.GeneralRulesActivity;
import com.example.shubhamverma.trafficetiquettes.Manifest;
import com.example.shubhamverma.trafficetiquettes.statecodes.StateCodesActivity;
import com.example.shubhamverma.trafficetiquettes.trafficsigns.TrafficSignsActivity;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;

public class EmergencyHelplinesFirebase {
    final Context c;
    String DB_URL;
    ListView listView;
    Firebase firebase;
    ArrayList<EmergencyHelplines> emergencyHelplines= new ArrayList<>();
    EmergencyHelplinesCustomAdapter customAdapter;
    int selectedPosition;

    public  EmergencyHelplinesFirebase(Context c, String DB_URL, ListView listView)
    {
        this.c= c;
        this.DB_URL= DB_URL;
        this.listView= listView;

        Firebase.setAndroidContext(c);
        firebase=new Firebase(DB_URL);
    }

    public  void refreshdata()
    {
        firebase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                getupdates(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                getupdates(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPosition =i;
                listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                Object e = customAdapter.getItem(i);
                EmergencyHelplines emergencyHelpline = (EmergencyHelplines) e;

                String phoneNumber = emergencyHelpline.getNumber();
                Intent intent = new Intent(Intent.ACTION_DIAL);
                String p = "tel:" + phoneNumber;
                intent.setData(Uri.parse(p));
                c.startActivity(intent);
            }
        });
    }

    public void getupdates(DataSnapshot dataSnapshot){

        emergencyHelplines.clear();

        for(DataSnapshot ds :dataSnapshot.getChildren()){
            EmergencyHelplines emergencyHelpline= new EmergencyHelplines();
            emergencyHelpline.setName(ds.getValue(EmergencyHelplines.class).getName());
            emergencyHelpline.setNumber(ds.getValue(EmergencyHelplines.class).getNumber());
            emergencyHelplines.add(emergencyHelpline);
        }

        if(emergencyHelplines.size()>0)
        {
            customAdapter=new EmergencyHelplinesCustomAdapter(c, emergencyHelplines);
            listView.setAdapter((ListAdapter) customAdapter);
        }else
        {
            Toast.makeText(c, "No data", Toast.LENGTH_SHORT).show();
        }
    }
}
