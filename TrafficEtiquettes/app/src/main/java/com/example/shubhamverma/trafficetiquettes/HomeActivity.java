package com.example.shubhamverma.trafficetiquettes;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.shubhamverma.trafficetiquettes.EmergenecyHelplines.EmergencyHelplinesActivity;
import com.example.shubhamverma.trafficetiquettes.MapsActivity;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private ListView mainMenuListItem;
    private Boolean itemSelected = false;
    private int selectedPosition = 0;
    private FirebaseDatabase database;
    private DatabaseReference dbref;

    DrawerLayout drawerLayout;


    ArrayList<String> listItems = new ArrayList<String>();
    ArrayList<String> listSerialNumber = new ArrayList<String>();

    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab= getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        //setupNavigationView();

        FirebaseApp.initializeApp(this);

        mainMenuListItem = (ListView) findViewById(R.id.mainMenuListView);
        database = FirebaseDatabase.getInstance();

        dbref = database.getReference("mainMenuListItems");

        adapter = new ArrayAdapter<String>(this,R.layout.textlistview,listItems);

        mainMenuListItem.setAdapter(adapter);
        mainMenuListItem.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        mainMenuListItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPosition = i;
                if(adapter.getItem(selectedPosition).equalsIgnoreCase("Know Your Location"))
                    startActivity(new Intent(HomeActivity.this,MapsActivity.class));
                else if(adapter.getItem(selectedPosition).equalsIgnoreCase(getResources().getString(R.string.generalRulesTitleText)))
                    startActivity(new Intent(HomeActivity.this,GeneralRulesActivity.class));
                else  if(adapter.getItem(selectedPosition).equalsIgnoreCase(getResources().getString(R.string.offencesAndFinesTitleText)))
                    startActivity(new Intent(HomeActivity.this,OffencesAndFinesActivity.class));
                else  if(adapter.getItem(selectedPosition).equalsIgnoreCase("Emergency Helplines"))
                    startActivity(new Intent(HomeActivity.this,EmergencyHelplinesActivity.class));
            }
        });
        addChildEventListener();
    }
    private void addChildEventListener() {
        final ProgressDialog mProgressDialog = new ProgressDialog(this);

        mProgressDialog.setMessage("Loading ...");
        mProgressDialog.show();

        ChildEventListener childListener = new ChildEventListener() {
            int count =0;
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                count++;
                adapter.add(
                        (String)dataSnapshot.child("Item").getValue());
                listSerialNumber.add(dataSnapshot.getKey());
                if(count >=dataSnapshot.getChildrenCount())
                    mProgressDialog.dismiss();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getKey();
                int index = listSerialNumber.indexOf(key);

                if(index != -1) {
                    listItems.remove(index);
                    listSerialNumber.remove(index);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        };
        dbref.addChildEventListener(childListener);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            case android.R.id.home:
                 if(drawerLayout != null)
                    drawerLayout.openDrawer(GravityCompat.START);
                 return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    private void setupNavigationView(){
//        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
//    }

}
