package com.example.shubhamverma.trafficetiquettes.trafficsigns;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.shubhamverma.trafficetiquettes.R;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;

public class TrafficSignsCustomAdapter extends BaseAdapter {
    Context c;
    ArrayList<TrafficSigns> trafficSigns;
    LayoutInflater inflater;
    FirebaseStorage storage;
    StorageReference storageReference;


    public TrafficSignsCustomAdapter(Context c, ArrayList<TrafficSigns> trafficSigns) {
        this.c = c;
        this.trafficSigns = trafficSigns;

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
    }

    @Override
    public int getCount() {
        return trafficSigns.size();
    }

    @Override
    public Object getItem(int i) {
        return trafficSigns.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertview, ViewGroup viewGroup) {

        if (inflater== null)
        {
            inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertview==null)
        {
            convertview= inflater.inflate(R.layout.traffic_signs_listview_layout,viewGroup,false);
        }

        final TrafficSignsHolder holder= new TrafficSignsHolder(convertview);

        holder.signName.setText(trafficSigns.get(i).getName());

        try {
            final File localFile = File.createTempFile("images", "jpg");
            storageReference = storage.getReferenceFromUrl("gs://traffic-etiquettes.appspot.com/TrafficSigns")
                    .child(trafficSigns.get(i).getUrl());

            storageReference.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                    holder.signImage.setImageBitmap(bitmap);
                    localFile.delete();
                }
            }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                }
            });
        }catch (IOException e)
        {}
        return convertview;
    }

}

