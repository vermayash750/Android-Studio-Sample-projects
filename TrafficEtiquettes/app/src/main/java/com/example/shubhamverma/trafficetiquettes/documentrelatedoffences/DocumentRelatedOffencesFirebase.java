package com.example.shubhamverma.trafficetiquettes.documentrelatedoffences;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;

public class DocumentRelatedOffencesFirebase {
    Context c;
    String DB_URL;
    ListView listView;
    Firebase firebase;
    int selectedPosition;
    ArrayList<DocumentRelatedOffences> documentRelatedOffences= new ArrayList<>();
    DocumentRelatedOffencesCustomAdapter customAdapter;

    public  DocumentRelatedOffencesFirebase(Context c, String DB_URL, ListView listView)
    {
        this.c= c;
        this.DB_URL= DB_URL;
        this.listView= listView;

        Firebase.setAndroidContext(c);
        firebase=new Firebase(DB_URL);
    }

    public  void refreshdata()
    {
        firebase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                getupdates(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                getupdates(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPosition =i;
                listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                Object obj = customAdapter.getItem(selectedPosition);
                DocumentRelatedOffences documentRelated = (DocumentRelatedOffences) obj;

                String section = documentRelated.getSection();
                String penalty = documentRelated.getPenalty();

                Intent intent = new Intent(c,DocumentRelatedOffencesDescriptionActivity.class);
                intent.putExtra("sectionText",section);
                intent.putExtra("penaltyText",penalty);

                c.startActivity(intent);
            }
        });

    }

    public void getupdates(DataSnapshot dataSnapshot){

        documentRelatedOffences.clear();

        for(DataSnapshot ds :dataSnapshot.getChildren()){
            DocumentRelatedOffences documentRelatedOffence= new DocumentRelatedOffences();

            documentRelatedOffence.setOffence(ds.getValue(DocumentRelatedOffences.class).getOffence());
            documentRelatedOffence.setPenalty(ds.getValue(DocumentRelatedOffences.class).getPenalty());
            documentRelatedOffence.setSection(ds.getValue(DocumentRelatedOffences.class).getSection());
           documentRelatedOffences.add(documentRelatedOffence);
        }

        if(documentRelatedOffences.size()>0)
        {
            customAdapter=new DocumentRelatedOffencesCustomAdapter(c, documentRelatedOffences);
            listView.setAdapter((ListAdapter) customAdapter);
        }else
        {
            Toast.makeText(c, "No data", Toast.LENGTH_SHORT).show();
        }
    }
}
