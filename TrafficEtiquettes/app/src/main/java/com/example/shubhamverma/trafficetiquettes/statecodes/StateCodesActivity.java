package com.example.shubhamverma.trafficetiquettes.statecodes;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.example.shubhamverma.trafficetiquettes.R;

public class StateCodesActivity extends AppCompatActivity {

    final static String DB_URL= "https://traffic-etiquettes.firebaseio.com/stateCodesListItems/";
    ListView listView;
    StateCodesFirebase firebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_codes);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView=(ListView)findViewById(R.id.listView);
        firebase= new StateCodesFirebase(this, DB_URL,listView);
        firebase.refreshdata();
    }
}
