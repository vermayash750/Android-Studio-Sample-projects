package com.example.shubhamverma.trafficetiquettes.statecodes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.shubhamverma.trafficetiquettes.R;

import java.util.ArrayList;

public class StateCodesCustomAdaper extends BaseAdapter {
    Context c;
    ArrayList<StateCodes> stateCodes;
    LayoutInflater inflater;

    public StateCodesCustomAdaper(Context c, ArrayList<StateCodes> stateCodes) {
        this.c = c;
        this.stateCodes = stateCodes;
    }

    @Override
    public int getCount() {
        return stateCodes.size();
    }

    @Override
    public Object getItem(int i) {
        return stateCodes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertview, ViewGroup viewGroup) {

        if (inflater== null) {
            inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertview==null) {
            convertview= inflater.inflate(R.layout.state_codes_listview_layout,viewGroup,false);

        }

        StateCodesHolder holder= new StateCodesHolder(convertview);
        holder.nameText.setText(stateCodes.get(i).getName());
        holder.codeText.setText(stateCodes.get(i).getCode());
        return convertview;
    }
}
