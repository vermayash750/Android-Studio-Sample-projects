package com.example.shubhamverma.trafficetiquettes.parkingrelatedoffences;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.shubhamverma.trafficetiquettes.R;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;

public class ParkingRelatedOffencesCustomAdapter extends BaseAdapter {
    Context c;
    ArrayList<ParkingRelatedOffences> parkingRelatedOffences;
    LayoutInflater inflater;
    FirebaseStorage storage;
    StorageReference storageReference;


    public ParkingRelatedOffencesCustomAdapter(Context c, ArrayList<ParkingRelatedOffences> documentRelatedOffences) {
        this.c = c;
        this.parkingRelatedOffences = documentRelatedOffences;

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
    }

    @Override
    public int getCount() {
        return parkingRelatedOffences.size();
    }

    @Override
    public Object getItem(int i) {
        return parkingRelatedOffences.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertview, ViewGroup viewGroup) {

        if (inflater== null)
        {
            inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertview==null)
        {
            convertview= inflater.inflate(R.layout.parking_related_offences_listview_layout,viewGroup,false);
        }

        final ParkingRelatedOffencesHolder holder= new ParkingRelatedOffencesHolder(convertview);

        holder.offenceText.setText(parkingRelatedOffences.get(i).getOffence());
        return convertview;
    }

}

