package com.example.shubhamverma.trafficetiquettes.EmergenecyHelplines;

public class EmergencyHelplines {
    private String name;

    private String number;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public EmergencyHelplines() {

    }
}
