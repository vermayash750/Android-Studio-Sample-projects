package com.example.shubhamverma.trafficetiquettes.parkingrelatedoffences;


import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;

public class ParkingRelatedOffencesFirebase {
    Context c;
    String DB_URL;
    ListView listView;
    Firebase firebase;
    int selectedPosition;
    ArrayList<ParkingRelatedOffences> parkingRelatedOffences= new ArrayList<>();
    ParkingRelatedOffencesCustomAdapter customAdapter;

    public  ParkingRelatedOffencesFirebase(Context c, String DB_URL, ListView listView)
    {
        this.c= c;
        this.DB_URL= DB_URL;
        this.listView= listView;

        Firebase.setAndroidContext(c);
        firebase=new Firebase(DB_URL);
    }

    public  void refreshdata()
    {
        firebase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                getupdates(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                getupdates(dataSnapshot);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPosition =i;
                listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                Object obj = customAdapter.getItem(selectedPosition);
                ParkingRelatedOffences parkingRelated = (ParkingRelatedOffences) obj;

                String section = parkingRelated.getSection();
                String penalty = parkingRelated.getPenalty();

                Intent intent = new Intent(c,ParkingRelatedOffencesDescriptionActivity.class);
                intent.putExtra("sectionText",section);
                intent.putExtra("penaltyText",penalty);

                c.startActivity(intent);
            }
        });

    }

    public void getupdates(DataSnapshot dataSnapshot){

        parkingRelatedOffences.clear();

        for(DataSnapshot ds :dataSnapshot.getChildren()){
            ParkingRelatedOffences parkingRelatedOffence= new ParkingRelatedOffences();

            parkingRelatedOffence.setOffence(ds.getValue(ParkingRelatedOffences.class).getOffence());
            parkingRelatedOffence.setPenalty(ds.getValue(ParkingRelatedOffences.class).getPenalty());
            parkingRelatedOffence.setSection(ds.getValue(ParkingRelatedOffences.class).getSection());
            parkingRelatedOffences.add(parkingRelatedOffence);
        }

        if(parkingRelatedOffences.size()>0)
        {
            customAdapter=new ParkingRelatedOffencesCustomAdapter(c, parkingRelatedOffences);
            listView.setAdapter((ListAdapter) customAdapter);
        }else
        {
            Toast.makeText(c, "No data", Toast.LENGTH_SHORT).show();
        }
    }
}
