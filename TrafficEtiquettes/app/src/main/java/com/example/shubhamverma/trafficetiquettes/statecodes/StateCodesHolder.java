package com.example.shubhamverma.trafficetiquettes.statecodes;

import android.view.View;
import android.widget.TextView;

import com.example.shubhamverma.trafficetiquettes.R;

public class StateCodesHolder {
    TextView nameText;
    TextView codeText;

    public StateCodesHolder(View itemView) {
        nameText = (TextView) itemView.findViewById(R.id.stateName);
        codeText =(TextView) itemView.findViewById(R.id.stateCode);
    }
}
