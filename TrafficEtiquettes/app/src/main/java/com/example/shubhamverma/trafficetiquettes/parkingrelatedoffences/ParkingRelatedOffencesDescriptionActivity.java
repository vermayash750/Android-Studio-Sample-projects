package com.example.shubhamverma.trafficetiquettes.parkingrelatedoffences;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.example.shubhamverma.trafficetiquettes.R;

public class ParkingRelatedOffencesDescriptionActivity extends AppCompatActivity {

    TextView sectionText;
    TextView penaltyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_related_offences_description);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String section = getIntent().getStringExtra("sectionText");
        String penalty = getIntent().getStringExtra("penaltyText");

        sectionText = (TextView) findViewById(R.id.sectionText);
        penaltyText = (TextView) findViewById(R.id.penaltyText);

        sectionText.setText(section);
        penaltyText.setText(penalty);
    }
}
