package com.example.shubhamverma.trafficetiquettes.documentrelatedoffences;

import android.view.View;
import android.widget.TextView;

import com.example.shubhamverma.trafficetiquettes.R;

public class DocumentRelatedOffencesHolder {
    TextView offenceText;

    public DocumentRelatedOffencesHolder(View itemView) {
        offenceText = (TextView) itemView.findViewById(R.id.offenceName);
    }
}
