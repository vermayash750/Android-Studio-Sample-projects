package com.example.shubhamverma.trafficetiquettes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.shubhamverma.trafficetiquettes.documentrelatedoffences.DocumentRelatedOffencesActivity;
import com.example.shubhamverma.trafficetiquettes.parkingrelatedoffences.ParkingRelatedOffencesActivity;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class OffencesAndFinesActivity extends AppCompatActivity {

    private ListView offencesAndFinesListItem;
    private Boolean itemSelected = false;
    private int selectedPosition = 0;
    private FirebaseDatabase database;
    private DatabaseReference dbref;


    ArrayList<String> listItems = new ArrayList<String>();
    ArrayList<String> listSerialNumber = new ArrayList<String>();

    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offences_and_fines);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FirebaseApp.initializeApp(this);

        offencesAndFinesListItem = (ListView) findViewById(R.id.offencesAndFinesListView);

        database = FirebaseDatabase.getInstance();

        dbref = database.getReference("offencesListItems");

        adapter = new ArrayAdapter<String>(this,R.layout.textlistview,listItems);

        offencesAndFinesListItem.setAdapter(adapter);
        offencesAndFinesListItem.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        offencesAndFinesListItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPosition = i;
                if(adapter.getItem(selectedPosition).equalsIgnoreCase("Documents Related Offences"))
                    startActivity(new Intent(OffencesAndFinesActivity.this, DocumentRelatedOffencesActivity.class));
                if(adapter.getItem(selectedPosition).equalsIgnoreCase("Parking Related Offences"))
                    startActivity(new Intent(OffencesAndFinesActivity.this, ParkingRelatedOffencesActivity.class));
            }
        });

        addChildEventListener();
    }
    private void addChildEventListener() {
        ChildEventListener childListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                adapter.add(
                        (String)dataSnapshot.child("Item").getValue());
                listSerialNumber.add(dataSnapshot.getKey());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getKey();
                int index = listSerialNumber.indexOf(key);

                if(index != -1) {
                    listItems.remove(index);
                    listSerialNumber.remove(index);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        dbref.addChildEventListener(childListener);
    }
}
