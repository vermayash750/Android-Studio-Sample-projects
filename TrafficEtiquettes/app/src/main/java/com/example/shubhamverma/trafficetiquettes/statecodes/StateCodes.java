package com.example.shubhamverma.trafficetiquettes.statecodes;

public class StateCodes {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public StateCodes() {
    }
    private String code;
}
