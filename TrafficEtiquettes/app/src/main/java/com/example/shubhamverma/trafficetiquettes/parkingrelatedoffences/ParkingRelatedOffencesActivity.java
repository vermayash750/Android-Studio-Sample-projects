package com.example.shubhamverma.trafficetiquettes.parkingrelatedoffences;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.shubhamverma.trafficetiquettes.R;

public class ParkingRelatedOffencesActivity extends AppCompatActivity {

    final static String DB_URL= "https://traffic-etiquettes.firebaseio.com/parkingRelatedOffencesListItems";
    ListView listView;
    ParkingRelatedOffencesFirebase firebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_related_offences);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView=(ListView)findViewById(R.id.listView);
        firebase= new ParkingRelatedOffencesFirebase(this, DB_URL,listView);
        firebase.refreshdata();
    }
}
