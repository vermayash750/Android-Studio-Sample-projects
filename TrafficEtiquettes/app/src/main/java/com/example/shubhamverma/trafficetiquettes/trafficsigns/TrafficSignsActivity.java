package com.example.shubhamverma.trafficetiquettes.trafficsigns;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.example.shubhamverma.trafficetiquettes.R;

public class TrafficSignsActivity extends AppCompatActivity {

    final static String DB_URL= "https://traffic-etiquettes.firebaseio.com/trafficSignsListItems/";
    ListView listView;
    TrafficSignsFirebase firebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traffic_signs);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView=(ListView)findViewById(R.id.listView);
        firebase= new TrafficSignsFirebase(this, DB_URL,listView);
        firebase.refreshdata();
    }
}
