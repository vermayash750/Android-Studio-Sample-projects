package com.example.shubhamverma.trafficetiquettes.trafficsigns;

public class TrafficSigns {

    private String name;

    private String url;

    //Todo String description  need to be added

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public TrafficSigns() {

    }
}
