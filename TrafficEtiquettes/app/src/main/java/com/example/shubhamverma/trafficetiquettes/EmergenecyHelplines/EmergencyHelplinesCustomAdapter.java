package com.example.shubhamverma.trafficetiquettes.EmergenecyHelplines;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.shubhamverma.trafficetiquettes.R;

import java.util.ArrayList;

public class EmergencyHelplinesCustomAdapter extends BaseAdapter {
    Context c;
    ArrayList<EmergencyHelplines> emergencyHelplines;
    LayoutInflater inflater;

    public EmergencyHelplinesCustomAdapter(Context c, ArrayList<EmergencyHelplines> emergencyHelplines) {
        this.c = c;
        this.emergencyHelplines = emergencyHelplines;
    }

    @Override
    public int getCount() {
        return emergencyHelplines.size();
    }

    @Override
    public Object getItem(int i) {
        return emergencyHelplines.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertview, ViewGroup viewGroup) {

        if (inflater== null) {
            inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertview==null) {
            convertview= inflater.inflate(R.layout.emergency_helplines_listview_layout,viewGroup,false);

        }

        EmergencyHelplinesHolder holder= new EmergencyHelplinesHolder(convertview);
        holder.nameText.setText(emergencyHelplines.get(i).getName());
        holder.numberText.setText(emergencyHelplines.get(i).getNumber());
        return convertview;
    }
}
