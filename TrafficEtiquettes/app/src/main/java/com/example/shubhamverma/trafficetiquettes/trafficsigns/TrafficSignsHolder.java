package com.example.shubhamverma.trafficetiquettes.trafficsigns;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shubhamverma.trafficetiquettes.R;

public class TrafficSignsHolder {

    TextView signName;
    ImageView signImage;

    public TrafficSignsHolder(View itemView) {
        signName = (TextView) itemView.findViewById(R.id.signNameText);
        signImage = (ImageView) itemView.findViewById(R.id.signImage);
    }
}
