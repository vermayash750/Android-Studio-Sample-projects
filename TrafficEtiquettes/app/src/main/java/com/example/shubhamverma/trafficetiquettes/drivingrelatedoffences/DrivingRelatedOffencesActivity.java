package com.example.shubhamverma.trafficetiquettes.drivingrelatedoffences;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.shubhamverma.trafficetiquettes.R;

public class DrivingRelatedOffencesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driving_related_offences);
    }
}
