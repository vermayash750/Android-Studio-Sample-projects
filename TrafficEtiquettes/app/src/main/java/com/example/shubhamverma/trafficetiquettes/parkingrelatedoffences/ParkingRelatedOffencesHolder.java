package com.example.shubhamverma.trafficetiquettes.parkingrelatedoffences;

import android.view.View;
import android.widget.TextView;

import com.example.shubhamverma.trafficetiquettes.R;

public class ParkingRelatedOffencesHolder {
    TextView offenceText;

    public ParkingRelatedOffencesHolder(View itemView) {
        offenceText = (TextView) itemView.findViewById(R.id.offenceName);
    }
}
