package com.example.shubhamverma.trafficetiquettes.documentrelatedoffences;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.shubhamverma.trafficetiquettes.R;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;

public class DocumentRelatedOffencesCustomAdapter extends BaseAdapter {
    Context c;
    ArrayList<DocumentRelatedOffences> documentRelatedOffences;
    LayoutInflater inflater;
    FirebaseStorage storage;
    StorageReference storageReference;


    public DocumentRelatedOffencesCustomAdapter(Context c, ArrayList<DocumentRelatedOffences> documentRelatedOffences) {
        this.c = c;
        this.documentRelatedOffences = documentRelatedOffences;

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
    }

    @Override
    public int getCount() {
        return documentRelatedOffences.size();
    }

    @Override
    public Object getItem(int i) {
        return documentRelatedOffences.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertview, ViewGroup viewGroup) {

        if (inflater== null)
        {
            inflater=(LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertview==null)
        {
            convertview= inflater.inflate(R.layout.document_related_offences_listview_layout,viewGroup,false);
        }

        final DocumentRelatedOffencesHolder holder= new DocumentRelatedOffencesHolder(convertview);

        holder.offenceText.setText(documentRelatedOffences.get(i).getOffence());
        return convertview;
    }

}

