package com.example.shubhamverma.trafficetiquettes.EmergenecyHelplines;

import android.view.View;
import android.widget.TextView;

import com.example.shubhamverma.trafficetiquettes.R;

public class EmergencyHelplinesHolder {
    TextView nameText;
    TextView numberText;

    public EmergencyHelplinesHolder(View itemView) {
        nameText = (TextView) itemView.findViewById(R.id.emergencyNameText);
        numberText =(TextView) itemView.findViewById(R.id.numberText);
    }
}
