package com.example.shubhamverma.glogin.utils;

/**
 * Created by shubham verma on 30-Mar-18.
 */

public class Constants {

    public static final String FIREBASE_URL = "https://glogin-6b820.firebaseio.com/";
    public static final String FIREBASE_LOCATION_USERS = "users";
    public static final String FIREBASE_URL_USERS = FIREBASE_URL + "/" + FIREBASE_LOCATION_USERS;
    public static final String FIREBASE_PROPERTY_TIMESTAMP = "timestamp";
}